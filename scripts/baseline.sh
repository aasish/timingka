#!/bin/bash

cat $1|awk -F'\t' '{if($1){split($5,a,"=");if(a[2]<=6)print "5";else if(a[2]>6 && a[2]<=12)print "4";else if(a[2]>12 && a[2]<=18)print "3";else if(a[2]>18 && a[2]<=24)print "2";else if(a[2]>24) print "1";}else{print $0}}'
