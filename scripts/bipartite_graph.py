#!/usr/bin/env python

import sys
import csv
def read_ds_entities(ds_grep):
    with open(ds_grep, 'r') as dsentfd:
        lines = dsentfd.readlines()
        route = {}
        neighborhood = {}
        stops = {}
        stops_abs = {}
        for line in lines:
            line = line.strip('\n')
            line_num, entity = line.strip().split('] (')
            entity = entity.replace(')', '').strip().replace(' ', '_')
            if 'route' in line_num:
                line_num = line_num.split(':')[0]
                try:
                    route[entity].append(int(line_num))
                except:
                    route[entity] = [int(line_num)]
            elif 'neighborhood' in line_num or 'monument' in line_num:
                line_num = line_num.split(':')[0]
                try:
                    neighborhood[entity].append(int(line_num))
                except:
                    neighborhood[entity] = [int(line_num)]
            elif 'stop_part' in line_num:
                line_num = int(line_num.split(':')[0])
                try:
                    stops[line_num].update(set([entity]))
                except:
                    stops[line_num] = set([entity])

        #print route.keys(), len(route)
        #print neighborhood.keys(), len(neighborhood)
        for line, stop in stops.items():
            stop_list = list(stop)
            if len(stop_list) == 1:
                if stop_list[0] in neighborhood:
                    neighborhood[stop_list[0]].append(line)
                else:
                    try:
                        stops_abs[stop_list[0]].append(line)
                    except:
                        stops_abs[stop_list[0]] = [line]
            else:
                stop_list.sort()
                stop_list = '_AND_'.join(stop_list)
                try: 
                    stops_abs[stop_list].append(line)
                except:
                    stops_abs[stop_list] = [line]

       # for r, lines in route.items():
       #     print '\t'.join([r, str(lines)])
        
       # for n, lines in neighborhood.items():
       #     print '\t'.join([n, str(lines)])
        
        for stop, lines in stops_abs.items():
            print '\t'.join([stop, str(lines)])
            
            
def read_dialog_state(ds_lego):
    with open(ds_lego, 'r') as dsfd:
        lines = dsfd.readlines()
        for line in lines:
            slots = line.split('\t')            
            print slots[0], slots[2].split('=')[1]
            

ds_lego, ds_grep = sys.argv[1:]
#read_dialog_state(ds_lego)
read_ds_entities(ds_grep)
