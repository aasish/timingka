#!/usr/bin/env python
from sklearn.metrics import f1_score, recall_score, precision_score
import fileinput
from sklearn.metrics import precision_recall_fscore_support
from sklearn.metrics import classification_report
y_true = []
y_pred = []
pos_label = '5'
for line in fileinput.input():
    line = line.strip('\n').replace(' ', '')
    
    if line:
        y_t, y_p = line.split('\t')
        if y_t and y_p:
            y_true.append(y_t)
            y_pred.append(y_p)

#print y_true, y_pred
print precision_score(y_true, y_pred, average='micro', pos_label=pos_label)*100, recall_score(y_true, y_pred, average='micro', pos_label=pos_label)*100, f1_score(y_true, y_pred, average='micro', pos_label=pos_label)*100
print precision_score(y_true, y_pred, average='macro', pos_label=pos_label)*100, recall_score(y_true, y_pred, average='macro',pos_label=pos_label)*100, f1_score(y_true, y_pred, average='macro',pos_label=pos_label)*100

print precision_score(y_true, y_pred, average=None, pos_label=pos_label)*100, recall_score(y_true, y_pred, average=None, pos_label=pos_label)*100, f1_score(y_true, y_pred, average=None, pos_label=pos_label)*100

print precision_score(y_true, y_pred, average='weighted', pos_label=pos_label)*100, recall_score(y_true, y_pred, average='weighted', pos_label=pos_label)*100, f1_score(y_true, y_pred, average='weighted', pos_label=pos_label)*100

print(classification_report(y_true, y_pred))
