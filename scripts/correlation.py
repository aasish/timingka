#!/usr/bin/env python
from scipy.stats import kendalltau, spearmanr, pearsonr
import fileinput

y_true = []
y_pred = []
for line in fileinput.input():
    line = line.strip('\n').replace(' ', '')
    
    if line:
        y_t, y_p = line.split('\t')
        if y_t and y_p:
            y_true.append(float(y_t))
            y_pred.append(float(y_p))

print 'pearson', pearsonr(y_true, y_pred)
print 'spearman', spearmanr(y_true, y_pred)
print 'kendal', kendalltau(y_true, y_pred)
