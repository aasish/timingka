#!/usr/bin/env python

import sys
from ast import literal_eval
import re
import math
import graph_tool.all as gt_all
import graph_tool.community as gt_com
import graph_tool.draw as gt_draw
import graph_tool.topology as gt_top
import graph_tool.centrality as gt_cen
from collections import defaultdict, OrderedDict

SCALE_FACTOR = 50


def jaccard_index(setA, setB):
    global SCALE_FACTOR
    index = len(set(setA)&set(setB))/float(len(set(setA)|set(setB)))
    return index*SCALE_FACTOR

def cosine_similarity(vecA, vecB):
    vecA = dict(vecA)
    vecB = dict(vecB)
    

    vecA_sum = 0.0
    vec_sum  = 0.0
    vecB_sum = 0.0

    lenA = len(vecA)
    lenB = len(vecB) 

    for k,v in vecA.items():
        vecA_sum += v**2
        if len(vecA) < len(vecB) and k in vecB:
            vec_sum += v*vecB[k]
    
    for k, v in vecB.items():
        vecB_sum += v**2
        if len(vecA) >= len(vecB) and k in vecA: 
            vec_sum += v*vecA[k]
    
    vecA_sum = math.sqrt(vecA_sum)
    vecB_sum = math.sqrt(vecB_sum)
    
    cos_sim = float(vec_sum)/float((vecA_sum)+(vecB_sum))
    return cos_sim


def build_network(ents):
    g = gt_all.Graph(directed=False)
    people_keys = ents
    len_people = len(people_keys)
    vlist = g.add_vertex(len_people)
    #interests
    vprop_interests = g.new_vertex_property('vector<string>')
    #name
    vprop_name = g.new_vertex_property('string')
    #common interests
    eprop_interests = g.new_edge_property('vector<string>')
    #num common interests
    eprop_weight = g.new_edge_property('double')
    
    for i in range(0, len_people):
        person1 = people_keys[i]
        vprop_interests[g.vertex(i)] = people[person1]
        vprop_name[g.vertex(i)] = person1
        person1_vert = g.vertex(i)
        for j in range(i+1, len_people):
            person2 = people_keys[j]
            person2_vert = g.vertex(j)
            edge_weight = jaccard_index(people[person1], people[person2])
            #print person1, person2, edge_weight, set(people[person1])&set(people[person2])
            if edge_weight > 0:
                edge = g.add_edge(person1_vert, person2_vert)
                eprop_weight[edge] = edge_weight
                eprop_interests[edge] = set(people[person1]) & set(people[person2])
            
    
    g.vertex_properties['interests'] = vprop_interests
    g.vertex_properties['name'] = vprop_name
    
    g.edge_properties['interests'] = eprop_interests
    g.edge_properties['weight'] = eprop_weight
    
    
    
    return g


def build_graph(ents, filter_edges=None):
    g = gt_all.Graph(directed=False)

    ent_keys = ents.keys()
    range_ent = len(ent_keys)

    vlist = g.add_vertex(range_ent)
    
    #vprop_routes = g.new_vertex_property('vector<string>')
    
    vprop_name = g.new_vertex_property('string')
    
    eprop_interests = g.new_edge_property('vector<string>')
    
    eprop_weight = g.new_edge_property('double')

    for i in range(range_ent):
        ent1 = ent_keys[i]
        ent_vals1 = dict(ents[ent1])
        vprop_name[g.vertex(i)] = ent1
        ent1_vert = g.vertex(i)
        for j in range(i+1, range_ent):
            ent2 = ent_keys[j]
            ent_vals2 = dict(ents[ent2])
            vprop_name[g.vertex(j)] = ent2
            ent2_vert = g.vertex(j)
            if filter_edges and set([ent1, ent2]) in filter_edges:
                continue
            value = cosine_similarity(ent_vals1, ent_vals2)
            if value > 0.25:
                edge = g.add_edge(ent1_vert, ent2_vert)
                eprop_weight[edge] = value
                eprop_interests[edge] =  set(ent_vals1.keys()).intersection(set(ent_vals2.keys()))
                #print ent1, ent2, value, set(ent_vals1.keys()).intersection(set(ent_vals2.keys()))
      
    g.vertex_properties['name'] = vprop_name
    g.edge_properties['routes'] = eprop_interests
    g.edge_properties['weight'] = eprop_weight

    return g

def build_graph2(stops, neighs):
    for stop in stops:
        stop_vals = dict(stops[stop])
        for neigh in neighs:
            neigh_vals = dict(neighs[neigh])
            
            value = cosine_similarity(stop_vals, neigh_vals)
            if value > 0.0:
                print stop, neigh, value, set(stop_vals.keys()).intersection(set(stop_vals.keys()))

            
def get_ds(entities, ds):
    ds_lines = open(ds).readlines()
    entity = open(entities).readlines()
    
    ent_dialogs = {}
    for ent in entity:
        ent = re.sub('\t+', '\t', ent)
        ent, l = ent.strip('\n').split('\t')
        l = literal_eval(l)
        s = set([int(ds_lines[d-1].split('\t')[0]) for d in l])
        ent_dialogs[ent] = s
    return ent_dialogs


def print_keywords_in_community(spins, g):
    person_dict = {}
#    person_interests = g.vertex_properties['interests']
    person_name = g.vertex_properties['name']
    vert_index = g.vertex_index
    for v in g.vertices():
        comm = spins[v]
#        v_interests = person_interests[v]
        v_name = person_name[v]
        v_id = vert_index[v]
        try:
            person_dict[comm].update(set([(v_name, v_id)]))
        except KeyError:
            person_dict[comm] = set([(v_name, v_id)])
        
    len_person_dict = len(person_dict)
    for comm in range(0,len_person_dict):
        d = defaultdict(int)
        print '\t'.join([str(comm),str(person_dict[comm])])


def get_ent_routes(ents, routes):
    ent_routes = {}
    for ent, ds_ent in ents.items():
        for route, ds_route in routes.items():
            jac = jaccard_index(ds_ent, ds_route)
            if jac == 0.0:
                continue
            try:
                ent_routes[ent].append((route, jac))
            except:
                ent_routes[ent] = [(route, jac)]
    return ent_routes

def get_filtered_edges(filter_edges):
    with open(filter_edges) as fd:
        lines = fd.readlines()
        lines = [set(line.split('\t')[0].split(' ')) for line in lines if line.strip('\n').split('\t')[1] == 'No']
        return lines

stops, routes, neighs, ds, filter_network = sys.argv[1:]
stops_ds = get_ds(stops, ds)
neighs_ds = get_ds(neighs, ds)
routes_ds = get_ds(routes, ds)


#stops_routes = get_ent_routes(stops_ds, routes_ds)
neighs_routes = get_ent_routes(neighs_ds, routes_ds)
filtered_edges = get_filtered_edges(filter_network)
#print filtered_edges
g = build_graph(neighs_routes, filtered_edges)
b = gt_com.community_structure(g, 10000, 5)
print gt_com.modularity(g, b)

#gt_draw.graph_draw(g, vertex_text=g.vertex_index, vertex_fill_color=b, output_size=(420, 420), output="n1.pdf")
#print_keywords_in_community(b, g)
#build_graph2(stops_routes, neighs_routes)
#print stops_routes



#build_graph(stops_routes)
