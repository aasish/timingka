#!/usr/bin/env python
import sys
import csv

def clean(data):
    return unicode(data, errors="ignore")

def load_lego(iqlego):
    """
    1. read feature csv 
    2. store the first line in a dictionary
    3. read each example's first col and see if it is a new example or not
    """
    with open(iqlego, 'r') as csvfile:
        lego_reader = csv.reader(csvfile, delimiter=';')
        examples = {}
        curr_example = None
        prev_example = None
        first_row = lego_reader.next()

        features = dict([(val, k) for k, val in enumerate(first_row[1:])])
        for row in lego_reader:
            try:
                examples[row[0]].append(row[0:])
            except:
                examples[row[0]] = [row[0:]]
        
        return features, examples


def select_feats(feat_map, reject=False):
    """
    ['Prompt', 'Utterance', 'ASRRecognitionStatus', '#ASRSuccess', '(#)ASRSuccess', '%ASRSuccess', '#TimeOutPrompts', '(#)TimeOutPrompts', '%TimeOutPrompts', '#ASRRejections', '(#)ASRRejections', '%ASRRejections', '#TimeOuts_ASRRej', '(#)TimeOuts_ASRRej', '%TimeOuts_ASRRej', 'Barged-In?', '#Barge-Ins', '(#)Barge-Ins', '%Barge-Ins', 'ASRConfidence', 'MeanASRConfidence', '(Mean)ASRConfidence', 'UTD', 'ExMo', 'Modality', 'UnExMo?', '#UnExMo', '(#)UnExMo', '%UnExMo', 'WPUT', 'WPST', 'SemanticParse', 'HelpRequest?', '#HelpRequests', '(#)HelpRequest', '%HelpRequest', 'Activity', 'ActivityType', 'DD', 'RoleIndex', 'RoleName', 'RePrompt?', '#RePrompts', '(#)RePrompts', '%RePrompts', 'LoopName', '#Exchanges', '#SystemTurns', '#UserTurns', '#SystemQuestions', '(#)SystemQuestions', 'SystemDialogueAct', 'UserDialogueAct', 'AudioFile', 'EmotionalState', 'IQRater1', 'IQRater2', 'IQRater3', 'IQMedian']

    
    ignore features: 
    Prompt, Utterance, (#)*, IQRater*, SemanticParse, Modality, UnExMo*
    """
    absolute_ignore = ['Prompt', 'Utterance', 'Modality', 'SemanticParse','AudioFile', 'HelpRequest?', 'EmotionalState', 'ASRRecognitionStatus','BargedIn?']
    use_feats = ['#ASRRejections', '%RePrompts', '#RePrompts', '#SystemQuestions', 'RoleIndex', '#TimeOutPrompts', '#TimeOuts_ASRRej', '#SystemTurns', '#UserTurns', '#HelpRequests', "ASRConfidence", "MeanASRConfidence"]
    #use_feats = ['Activity','SemanticParse']
    target_feats = []
    if reject:
        for feat in feat_map:
            if feat in absolute_ignore or feat.startswith('(#)') or feat.startswith('IQ') or 'UnExMo' in feat:
                continue

            else:
                target_feats.append((feat_map[feat],feat))

    else:
        for feat in feat_map:
            if feat in use_feats:
                target_feats.append((feat_map[feat], feat))
                
    return target_feats
     
def gen_crf_feat(feat_map, examples):

    target_feats = select_feats(feat_map)

    for example_key in examples.keys():
        iq_val = None
        example = examples[example_key]
        for exchange in example:
            if exchange[-1]:
                iq_val = exchange[-1]
            elif exchange[-2]:
                iq_val = exchange[-2]
            elif exchange[-3]:
                iq_val = exchange[-3]
            elif exchange[-4]:
                iq_val = exchange[-4]

            if not iq_val:
                break

            feat_vec = []
            feat_vec.append(example_key)
            #feat_vec.append(exchange[0])
            #if not exchange[1]:
            #    continue
            
            exchange[1] = clean(exchange[1].strip())
            
            exchange[2] = clean(exchange[2].strip())
            
            feat_vec.append(exchange[1])
            feat_vec.append(exchange[2])

            for feat in target_feats:
                feat_val = exchange[feat[0]].strip('\t').strip(' ')
                if feat_val:
                    pass
                else:
                    feat_val = 'default'
                    
                feat_cat = feat[1].replace('?','Q').replace('%','P').replace('#','N').replace('(','').replace(')','').replace('-','')
                try:
                    feat_val = clean(feat_val)
                except:
                    pass
                feat_vec.append('%s=%s' %(feat_cat,feat_val))
            if iq_val:
                feat_vec.append(iq_val)
            print '\t'.join(feat_vec)
        
        if not iq_val:
           continue
        else:
            print ''

            
iqlego = sys.argv[1]
features, examples = load_lego(iqlego)
gen_crf_feat(features, examples)
