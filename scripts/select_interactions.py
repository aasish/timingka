#!/usr/bin/env python

import sys

def select_interactions(interactions, ids):
    ids = dict((i.strip('\n'), "") for i in ids)
    for i, interaction in enumerate(interactions):
        if '\t' in interaction:
            if interaction.split('\t')[0] in ids:
                print interaction.strip('\n')
                try:
                    if '\t' not in interactions[i+1]:
                        print '\n'
                except:
                    pass
            
interaction, ids = sys.argv[1:]
interactions = open(interaction).readlines()
ids_lines = open(ids).readlines()
select_interactions(interactions, ids_lines)
