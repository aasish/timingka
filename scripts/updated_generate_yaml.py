#!/usr/bin/env python

import argparse
import copy
import textwrap
import yaml
from ast import literal_eval

template = {'testvox_config': { 'base_media_directory': 'media',
                            'pagetitle': 'EventSpeak',
                            'rec_url' : 'http://sociro.speech.cs.cmu.edu:9000'},
            'testvox_steps': [{'name':'exit_survey', 
                               'data': {'filename':'data01.mp3'},
                               'task_type' : 'surveyform', 
                               'instruction':''}]}


questions = [{'name': 'conv_rate',
             'type': 'select',
             'text': 'How do you rate the quality of this interaction?',
              'options': ['5(Good)',
                          '4' ,
                          '3',
                          '2',
                          '1(Bad)']},
             {'name': 'would_you',
             'type': 'select',
             'text': 'A survey question at the end of this interaction may annoy the user?',
              'options': ['Yes',
                          'No' ]},
             {'name': 'route',
              'type': 'select',
              'text': '',
              'options': ['Yes',
                          'No' ,
                          'I dont know']}]

encode = lambda s: s.encode('utf-8')


question_template = 'S: This is an unrelated question to the interaction above. <br> But Could you tell me whether %(neigh1)s and %(neigh2)s neighborhoods are connected by anyone of these routes [ %(route)s ] ? You can use google maps to verify.'

class literal_unicode(unicode): pass

def literal_unicode_presenter(dumper, data):
    return dumper.represent_scalar(u'tag:yaml.org,2002:str', data, style='>')

yaml.add_representer(literal_unicode, literal_unicode_presenter)

class literal(str): pass

def literal_presenter(dumper, data):
    return dumper.represent_scalar('tag:yaml.org,2002:str', data, style='>')

yaml.add_representer(literal, literal_presenter)

wrapper = textwrap.TextWrapper(width=90,break_long_words=False,replace_whitespace=False)

    
def generate_yaml(inputfile, outputfile, questions_routes, num_interactions):
    global template, questions
    data = copy.deepcopy(template)
    file_i = 0
    outputf = '%(output)s_%(id)d.yaml' 
    question_id = 0
#    outputfname = outputf % {'id' : file_i+1, 'output':outputfile.split('.')[0]}


    with open(inputfile) as infd:
        
        
        lines = infd.readlines()
        interaction_id = 0
        interaction_instr = []
        #data['testvox_steps'].append({})
        for i, line in enumerate(lines):

            line = line.strip('\n').strip(' ')
            slots = line.split('\t')
            
            try:
                if '\t' not in line:

                    if interaction_instr:
                        
                        data['testvox_steps'][interaction_id]['name'] = 'exit_survey%d' %(interaction_id)
                        data['testvox_steps'][interaction_id]['task_type'] = 'surveyform'
                        data['testvox_steps'][interaction_id]['instruction'] = ''.join(interaction_instr)
                        data['testvox_steps'][interaction_id]['questions'] = copy.deepcopy(questions)
                        #print data['testvox_steps'][interaction_id]['questions'][2]
                        neigh1 = questions_routes[question_id][0].replace('_', ' ')
                        neigh2 = questions_routes[question_id][1].replace('_', ' ')
                        routes = ', '.join(list(eval(questions_routes[question_id][3])))
                        data['testvox_steps'][interaction_id]['questions'][0]['name'] = 'conv_rate%d' %(interaction_id)
                        data['testvox_steps'][interaction_id]['questions'][1]['name'] = 'would_you%d' %(interaction_id)
                        data['testvox_steps'][interaction_id]['questions'][2]['name'] = 'route%d' %(interaction_id)
                        data['testvox_steps'][interaction_id]['questions'][2]['text'] = question_template % {'neigh1' : neigh1, 'neigh2' : neigh2, 'route' : routes}

                        if question_id < len(questions_routes):
                            question_id += 1

                        data['testvox_steps'][interaction_id]['data'] = [{'filename':'data01.mp3'}]
                        interaction_instr = []
                        interaction_id += 1
                        

                        if interaction_id >= num_interactions:
                            
                            file_i += 1
                            interaction_id = 0
                            outputfname = outputf % {'id' : file_i, 'output':args.outputfile.split('.')[0]}
                            outfd = open(outputfname, 'w')
                            outfd.write(yaml.dump(data, allow_unicode=True, default_flow_style=False))
                            
                            outfd.close()
                            data = copy.deepcopy(template)
                        
                        else:    
                            data['testvox_steps'].append({})
                    continue
            except:
                pass
            
            if len(slots) > 1:
                slots[1] = slots[1].strip()
                slots[2] = slots[2].strip()
                if slots[1] and slots[2]:
                    interaction_instr.append('S: %s <br> U: %s <br>' %(slots[1], slots[2]))
                else:
                    interaction_instr.append('S: %s <br>' %(slots[1]))
                
            
            
        # if data:
        #     file_i += 1
        #     interaction_id = 0
        #     outputfname = outputf % {'id' : file_i, 'output':args.outputfile.split('.')[0]}
        #     outfd = open(outputfname, 'w')
        #     outfd.write(yaml.dump(data, allow_unicode=True, default_flow_style=False))
        #     outfd.close()
                
    

                    
                    

            
def load_yaml(template):
    with open(template) as tempfd:
        data_template =  yaml.load(tempfd)
        return data_template
    return None

                
def load_csv(textfile):
    textlines = []
    with open(textfile) as textfd:
        textlines = textfd.readlines()
        textlines = [line.strip('\n').split(' ') for line in textlines]

    return textlines

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', 
                        '--inputfile',
                        type=str,
                        help='-i inputf',
                        required=True
                    )

    parser.add_argument('-t', 
                        '--template',
                        help='-t template',
                        required=True
                    )

    parser.add_argument('-o', 
                        '--outputfile',
                        help='-o outputf',
                        required=True
                    )


    args = parser.parse_args()
    
    questions_routes = load_csv(args.template)
    
    generate_yaml(args.inputfile, args.outputfile, questions_routes, 10)
    
